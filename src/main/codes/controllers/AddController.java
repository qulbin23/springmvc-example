package controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import services.AddService;

@Controller
public class AddController {
	@RequestMapping("/add")
	public ModelAndView add(HttpServletRequest request, HttpServletResponse response) {
		int i = Integer.parseInt(request.getParameter("input1"));
		int j = Integer.parseInt(request.getParameter("input2"));

		AddService service = new AddService();
		int hasil = service.add(i, j);

		ModelAndView modelView = new ModelAndView();
		modelView.setViewName("addSuccess.jsp");
		modelView.addObject("result", hasil);

		return modelView;
	}
}
