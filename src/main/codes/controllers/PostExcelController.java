package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.util.IOUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import helpers.ExcelPoiHelper;
import helpers.MyCell;

@Controller
public class PostExcelController {
	private String fileLocation;

	@PostMapping("/uploadExcelFile")
	public ModelAndView uploadFile(Model model, MultipartFile file) throws IOException {
		File filex = new File("C:/Users/CoolBee/Downloads/test/cek2.xlsx");
		FileInputStream input = new FileInputStream(filex);
		file = new MockMultipartFile ("filex", filex.getName(), "text/plain", IOUtils.toByteArray(input));
		
		InputStream in = file.getInputStream();
		
		InputStreamResource resource = new InputStreamResource(in);
		
		
		
//		InputStream in = new FileInputStream(new File("C:/Users/CoolBee/Downloads/test/cek.xlsx"));
		
		
		File currDir = new File(".");
		String path = currDir.getAbsolutePath();
		fileLocation = path.substring(0, path.length() - 1) + file.getOriginalFilename();
//		fileLocation = "C:\\Users\\CoolBee\\Downloads\\test";
		FileOutputStream f = new FileOutputStream(fileLocation);
		int ch = 0;
		while ((ch = in.read()) != -1) {
			f.write(ch);
		}
		f.flush();
		f.close();
		model.addAttribute("message", "File: " + file.getOriginalFilename() + " has been uploaded successfully!");
//		model.addAttribute("message", "File: huhuhu " +  " has been uploaded successfully!");
		System.out.println("sukses upload excel");
		
		ModelAndView modelView = new ModelAndView();
		modelView.setViewName("pesanExcel.jsp");
		modelView.addObject(model);
		return modelView;
	}

	@Resource(name = "excelPoiHelper")
	private ExcelPoiHelper excelPoiHelper;

	@RequestMapping(method = RequestMethod.GET, value = "/readPOI")
	public ModelAndView readPOI(Model model) throws IOException {
		if (fileLocation != null) {
			if (fileLocation.endsWith(".xlsx") || fileLocation.endsWith(".xls")) {
				Map<Integer, List<MyCell>> data = excelPoiHelper.readExcel(fileLocation);
				model.addAttribute("data", data);
			} else {
				model.addAttribute("message", "Not a valid excel file!");
			}
		} else {
			model.addAttribute("message", "File missing! Please upload an excel file.");
		}
		System.out.println("readPOI keprint");
		
		ModelAndView modelView = new ModelAndView();
		modelView.addObject(model);
		modelView.setViewName("readExcel.jsp");
		return modelView;
	}

}
